/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_idle_state.h
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_IDLE_STATE_H
#define __RE_IDLE_STATE_H

#include "re_std_def.h"
#include "re_can_int_callback.h"
#include "re_timer_int_callback.h"
#include "re_can_init.h"
#include "can/re_app_can.h"
#include "re_uart_init.h"
#include "stdio.h"
#include "uart/re_app_UART_Ring.h"

RE_StatusTypeDef RE_Idle_State_Handler(void);

extern uint8_t bat_num;
extern uint32_t BifrostId;
extern uint8_t Rpi_Data_Buffer[50];
extern uint16_t Rpi_Data_Buffer_len;

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
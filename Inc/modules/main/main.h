/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   main.h
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_it.h"
#include "re_std_def.h"
#include "re_sys_clk_config.h"
#include "re_can_init.h"
#include "re_uart_init.h"
#include "re_timer_init.h"
#include "idle_state/re_idle_state.h"
#include "can/re_app_can.h"
#include "uart/re_app_UART_Ring.h"

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/

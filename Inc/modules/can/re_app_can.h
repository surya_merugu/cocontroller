/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_app_can.h
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_APP_CAN_H
#define __RE_APP_CAN_H

#include "string.h"
#include "re_can_init.h"

typedef struct __attribute__((packed))
{
    uint32_t CanID;
    uint8_t PhyID[7];
}BifrostInfo_t;

extern uint8_t unlockbifrostlatch[8];
extern BifrostInfo_t BifrostInfo[4];
extern uint8_t Charger_Msg[8];

RE_StatusTypeDef RE_Req_DockData(uint32_t Bifrost_CANId);
RE_StatusTypeDef RE_Unlock_Latch(void);
RE_StatusTypeDef RE_Req_Bifrost_Id(void);
RE_StatusTypeDef RE_Capture_DockData(void);
RE_StatusTypeDef RE_Charger_InfoMsgs(void);

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
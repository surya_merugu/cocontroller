/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_timer_init.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_init.h"

TIM_HandleTypeDef htim2_t, htim3_t;

/**
 * @brief Timer Base Init
 * This function configures clock and set priority for the timer
 * @param  timer handle typedef
 * @retval Exit status
 */
void HAL_TIM_Base_MspInit (TIM_HandleTypeDef* htim_base)
{   
    if(htim_base->Instance == TIM2)
    {
        __HAL_RCC_TIM2_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM2_IRQn, 1, 0);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);
    }
    else if(htim_base->Instance == TIM3)
    {
        __HAL_RCC_TIM3_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);        
    }
}

/**
 * @brief Timer Initialization
 * This function configures the internal timer
 * @param  void
 * @retval Exit status
 */
RE_StatusTypeDef RE_TIMER2_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim2_t.Instance                             = TIM2;
    htim2_t.Init.Prescaler                       = 47999;
    htim2_t.Init.CounterMode                     = TIM_COUNTERMODE_UP;
    htim2_t.Init.Period                          = 874;
    htim2_t.Init.ClockDivision                   = TIM_CLOCKDIVISION_DIV1;
    htim2_t.Init.AutoReloadPreload               = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sClockSourceConfig.ClockSource             = TIM_CLOCKSOURCE_INTERNAL;
    HAL_TIM_ConfigClockSource(&htim2_t, &sClockSourceConfig);
    sMasterConfig.MasterOutputTrigger          = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode              = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
 * @brief Timer Initialization
 * This function configures the internal timer
 * @param  void
 * @retval Exit status
 */
RE_StatusTypeDef RE_TIMER3_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim3_t.Instance                             = TIM3;
    htim3_t.Init.Prescaler                       = 49999;
    htim3_t.Init.CounterMode                     = TIM_COUNTERMODE_UP;
    htim3_t.Init.Period                          = 8399;
    htim3_t.Init.ClockDivision                   = TIM_CLOCKDIVISION_DIV1;
    htim3_t.Init.AutoReloadPreload               = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim3_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sClockSourceConfig.ClockSource             = TIM_CLOCKSOURCE_INTERNAL;
    HAL_TIM_ConfigClockSource(&htim3_t, &sClockSourceConfig);
    sMasterConfig.MasterOutputTrigger          = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode              = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef Led_Gpio_Init(void)
{
    GPIO_InitTypeDef LED_pin;
    __HAL_RCC_GPIOD_CLK_ENABLE();

    LED_pin.Pin               = GPIO_PIN_15;
    LED_pin.Mode              = GPIO_MODE_OUTPUT_PP;
    LED_pin.Speed             = GPIO_SPEED_FAST;
    LED_pin.Pull              = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD, &LED_pin);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/

/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   main.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#include "main/main.h"

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    HAL_Init();
    RE_SystemClock_Config();
    Led_Gpio_Init();
    RE_CAN1_Init();
    RE_CAN1_Filter_Config();
    RE_CAN1_Start_Interrupt();
    RE_UART2_Init();
    Ringbuf_Init();
    RE_TIMER2_Init();
    RE_TIMER3_Init();
    RE_Req_Bifrost_Id();
    if(HAL_TIM_Base_Start_IT(&htim3_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    while (1)
    {
        RE_Idle_State_Handler();
    }
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/
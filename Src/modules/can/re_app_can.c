/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_app_can.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/

#include "can/re_app_can.h"
BifrostInfo_t BifrostInfo[4];

#define GET_BIFROST_ID_CMD         0x09
#define CAPTURE_DOCK_INFO_CMD      0x5E
#define RESPOND_DOCK_DATA          0x5F

uint8_t unlockbifrostlatch[8];
uint8_t Charger_Msg[8];

RE_StatusTypeDef RE_Req_DockData(uint32_t Bifrost_CANId)
{
    uint32_t TxMailbox;
    uint8_t tx_msg[8] = {0,0,0,0,0,0,0,0};
    tx_msg[0] =  RESPOND_DOCK_DATA;
    tx_msg[1] = (Bifrost_CANId & 0xFF);
    tx_msg[2] = ((Bifrost_CANId >> 8)  & 0xFF);
    tx_msg[3] = ((Bifrost_CANId >> 16) & 0xFF);
    tx_msg[4] = ((Bifrost_CANId >> 24) & 0xFF);
    TxHeader_t.DLC   = 5;
    TxHeader_t.ExtId = 0x1;
    TxHeader_t.IDE   = CAN_ID_EXT;
    TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, tx_msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;
}

RE_StatusTypeDef RE_Unlock_Latch(void)
{
    uint32_t TxMailbox;
    TxHeader_t.DLC   = 8;
    TxHeader_t.ExtId = 0x2;
    TxHeader_t.IDE   = CAN_ID_EXT;
    TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, unlockbifrostlatch,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;
}

RE_StatusTypeDef RE_Req_Bifrost_Id(void)
{
    uint32_t TxMailbox;
    uint8_t tx_msg   = GET_BIFROST_ID_CMD;
    TxHeader_t.DLC   = 1;
    TxHeader_t.ExtId = 0x1;
    TxHeader_t.IDE   = CAN_ID_EXT;
    TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, &tx_msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;    
}

RE_StatusTypeDef RE_Capture_DockData(void)
{
    uint32_t TxMailbox;
    uint8_t tx_msg   = CAPTURE_DOCK_INFO_CMD;
    TxHeader_t.DLC   = 1;
    TxHeader_t.ExtId = 0x1;
    TxHeader_t.IDE   = CAN_ID_EXT;
    TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, &tx_msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }     
    return RE_OK;
}

RE_StatusTypeDef RE_Charger_InfoMsgs(void)
{
    uint32_t TxMailbox;
    TxHeader_t.DLC   = 8;
    TxHeader_t.ExtId = 0x3;
    TxHeader_t.IDE   = CAN_ID_EXT;
    TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, Charger_Msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }     
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/
/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_idle_state.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/
#include "idle_state/re_idle_state.h"

uint8_t bat_num = 0;
uint32_t BifrostId;
uint8_t t5sec_timer_count = 0;
uint8_t Rpi_Data_Buffer[50];
uint16_t Rpi_Data_Buffer_len;

RE_StatusTypeDef RE_Idle_State_Handler(void)
{
    if(Tx_Latch_Open_Detection_Flag == true)
    {
        Tx_Latch_Open_Detection_Flag = false;
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
        HAL_UART_Transmit(&huart2_t, Rpi_Data_Buffer, Rpi_Data_Buffer_len, 100);   
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));        
    }
    if(Transmit_RegData_Flag == true)
    {
        Transmit_RegData_Flag = false;
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
        HAL_UART_Transmit(&huart2_t, Rpi_Data_Buffer, Rpi_Data_Buffer_len, 100);   
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
    }
    if(Tx_Latch_Ctrl_Ack == true)
    {
        Tx_Latch_Ctrl_Ack = false;
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
        HAL_UART_Transmit(&huart2_t, Rpi_Data_Buffer, Rpi_Data_Buffer_len, 100); 
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));    
    }
    if(Tx_Charger_Ctrl_Ack == true)
    {
        Tx_Charger_Ctrl_Ack = false;
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
        HAL_UART_Transmit(&huart2_t, Rpi_Data_Buffer, Rpi_Data_Buffer_len, 100); 
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
    }
    if(t5sec_Elapsed == true)
    {
        t5sec_Elapsed = false;
        if(t5sec_timer_count == 1)
        {
            RE_Capture_DockData();
            memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
            sprintf((char*)Rpi_Data_Buffer, "%s%d%s%s\n", "@", 13, ";", "CTS");
            Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
            HAL_UART_Transmit(&huart2_t, Rpi_Data_Buffer, Rpi_Data_Buffer_len, 100);
            memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
        }
        else if(t5sec_timer_count == 2)
        {
            if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
        }
        t5sec_timer_count++;
        if(t5sec_timer_count > 2)
        {
            t5sec_timer_count = 0;
        }
    }
    if(t500ms_Elapsed == true)
    {
        t500ms_Elapsed = false;     
        uint16_t BifrostDataBuffer_len = 0;
        if(BifrostDataBufferEmpty == false)
        {
            BifrostDataBuffer_len = strlen((char *)BifrostData_Buffer);
            BifrostData_Buffer[BifrostDataBuffer_len - 1] = '\n';
            if(HAL_UART_Transmit(&huart2_t, BifrostData_Buffer, BifrostDataBuffer_len, 1000) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
            memset(BifrostData_Buffer, 0, sizeof(BifrostData_Buffer));
            BifrostDataBufferEmpty = true;
        }
        memset(BifrostData_Buffer, 0, sizeof(BifrostData_Buffer));
        BifrostId  = BifrostInfo[bat_num].CanID;
        sprintf((char *)BifrostData_Buffer, "%s%d%s%d%s%s","@", 14, ";", BifrostId, ";", " ");
        RE_Req_DockData(BifrostInfo[bat_num].CanID);
        BifrostId  = BifrostInfo[bat_num].CanID;
        bat_num++;
        if(bat_num > 3)
        {
            bat_num = 0;
            if(HAL_TIM_Base_Stop_IT(&htim2_t) != HAL_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
        }
    }
    if(Uart2RxCmplt == true)
    {
        RE_UART2_RxDataHandler();
        Uart2RxCmplt = false;
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/
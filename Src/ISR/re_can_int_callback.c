/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_can_int_callback.h"
    
/**
  ==============================================================================
                          ##### Callback functions #####
  ==============================================================================
    [..]
    This subsection provides the following callback functions:
      ( ) HAL_CAN_TxMailbox0CompleteCallback
      ( ) HAL_CAN_TxMailbox1CompleteCallback
      ( ) HAL_CAN_TxMailbox2CompleteCallback
      ( ) HAL_CAN_TxMailbox0AbortCallback
      ( ) HAL_CAN_TxMailbox1AbortCallback
      ( ) HAL_CAN_TxMailbox2AbortCallback
      (+) HAL_CAN_RxFifo0MsgPendingCallback
      ( ) HAL_CAN_RxFifo0FullCallback
      ( ) HAL_CAN_RxFifo1MsgPendingCallback
      ( ) HAL_CAN_RxFifo1FullCallback
      ( ) HAL_CAN_SleepCallback
      ( ) HAL_CAN_WakeUpFromRxMsgCallback
      ( ) HAL_CAN_ErrorCallback
  ==============================================================================
*/
uint8_t BifrostData_Buffer[800];
bool BifrostDataBufferEmpty = true;
static uint8_t bifrost_can_num = 0, bifrost_phy_num = 0;
uint8_t Pack_Reg_DataBuffer[50];
uint16_t Len_Pack_Reg_DataBuffer;
bool Transmit_RegData_Flag;
bool Tx_Latch_Ctrl_Ack;
bool Tx_Charger_Ctrl_Ack;
bool Tx_Latch_Open_Detection_Flag;

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    uint8_t rcvd_msg[8];
    char strBuffer[50];
    uint32_t Pack_CanId = 0;
    if(HAL_CAN_GetRxMessage(&hcan1_t, CAN_RX_FIFO0, &RxHeader_t, rcvd_msg) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if(RxHeader_t.ExtId == 0x7CF)
    {
        if(rcvd_msg[0] == 0x01)
        {
            BifrostInfo[bifrost_can_num].CanID = (rcvd_msg[1]) | ((rcvd_msg[2]) << 8) | ((rcvd_msg[3]) << 16) |\
                                                 ((rcvd_msg[4]) << 24);
            if(bifrost_can_num >= 3)
            {
                bifrost_can_num = 0;
            }  
            bifrost_can_num++;
        }
        else if(rcvd_msg[0] == 0x02)
        {
            memcpy(BifrostInfo[bifrost_phy_num].PhyID, rcvd_msg + 1, 7);
            if(bifrost_phy_num >= 3)
            {
                bifrost_phy_num = 0;
            }  
            bifrost_phy_num++;
        }
    }
    else if(RxHeader_t.ExtId % 100 == 10)
    {
        if(rcvd_msg[0] == 11)
        {
            Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
            if(rcvd_msg[1] != 0)
            {
                for(uint8_t i = 1; i <= 7; i++)
                {
                    Rpi_Data_Buffer[Rpi_Data_Buffer_len] = rcvd_msg[i];
                    Rpi_Data_Buffer_len++;
                }
                strcat((char *)Rpi_Data_Buffer, "\n");
            }
            else
            {
                strcat((char *)Rpi_Data_Buffer, "0");
                strcat((char *)Rpi_Data_Buffer, "\n");                
            }
            Transmit_RegData_Flag = true;
        }
        else if(rcvd_msg[0] == 12)
        {
            memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
            memset(strBuffer, 0, sizeof(strBuffer));
            Pack_CanId = (rcvd_msg[3]) | ((rcvd_msg[4]) << 8) | ((rcvd_msg[5]) << 16) | ((rcvd_msg[6]) << 24);
            sprintf(strBuffer, "%s%d%s%d%s%d,%d,%d,", "@", 11, ";", RxHeader_t.ExtId - 10, ";", rcvd_msg[1], rcvd_msg[2],\
                    Pack_CanId);
            strcat((char *)Rpi_Data_Buffer, (char *)strBuffer);
        }
    }
    else if(RxHeader_t.ExtId % 100 == 9)
    {
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
        memset(strBuffer, 0, sizeof(strBuffer));
        sprintf(strBuffer, "%s%d%s%d%s", "@", 15, ";", RxHeader_t.ExtId - 9, ";");
        strcat((char *)Rpi_Data_Buffer, strBuffer);
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer);
        for(uint8_t i = 0; i < RxHeader_t.DLC; i++)
        {
            char temp;
            sprintf(&temp, "%d", rcvd_msg[i]);
            Rpi_Data_Buffer[Rpi_Data_Buffer_len] = temp;
            Rpi_Data_Buffer_len++;
            Rpi_Data_Buffer[Rpi_Data_Buffer_len] = ',';
            Rpi_Data_Buffer_len++;
        } 
        Rpi_Data_Buffer_len = strlen((char*)Rpi_Data_Buffer); 
        Rpi_Data_Buffer[Rpi_Data_Buffer_len -1]   = '\n'; 
        Tx_Latch_Ctrl_Ack = true;
    }
    else if(RxHeader_t.ExtId % 100 == 8)
    {
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
        sprintf((char *)Rpi_Data_Buffer, "%s%d%s%d%s%d,%d\n", "@", 16, ";", RxHeader_t.ExtId - 8, ";", rcvd_msg[0],\
                rcvd_msg[1]);
        Tx_Charger_Ctrl_Ack = true;
    }
    else if(RxHeader_t.ExtId % 100 == 7)
    {
        memset(Rpi_Data_Buffer, 0, sizeof(Rpi_Data_Buffer));
        sprintf((char *)Rpi_Data_Buffer, "%s%d%s%d%s%d,%d,%d,%d\n", "@", 11, ";", RxHeader_t.ExtId - 7, ";", rcvd_msg[0],\
                rcvd_msg[1], 0, 0);   
        Tx_Latch_Open_Detection_Flag = true;
    }
    else if(RxHeader_t.ExtId == BifrostId + 4)
    {
        sprintf(strBuffer, "%x,%x,%x,", ((rcvd_msg[1]<<8)|(rcvd_msg[0])), ((rcvd_msg[3]<<8)|(rcvd_msg[2])),\
                ((rcvd_msg[5]<<8)|(rcvd_msg[4])));
        strcat((char *)BifrostData_Buffer, strBuffer);
        memset(strBuffer, 0, sizeof(strBuffer));
        BifrostDataBufferEmpty = false;
    }
    else if(RxHeader_t.ExtId == BifrostId + 3)
    { 
        uint16_t BifrostDataBuffer_len = 0;
        BifrostDataBuffer_len = strlen((char *)BifrostData_Buffer);
        BifrostData_Buffer[BifrostDataBuffer_len - 1] = '#';
        Pack_CanId = (rcvd_msg[0]) | ((rcvd_msg[1]) << 8) | ((rcvd_msg[2]) << 16) | ((rcvd_msg[3]) << 24);
        sprintf(strBuffer, "%x,%x,%x,%x,", rcvd_msg[5], rcvd_msg[6], rcvd_msg[7], Pack_CanId);    
        strcat((char *)BifrostData_Buffer, strBuffer);         
        memset(strBuffer, 0, sizeof(strBuffer));
    }
}

/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/

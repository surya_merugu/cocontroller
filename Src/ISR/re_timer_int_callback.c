/**
  *****************************************************************************
  * Title                 :   BSS_CoController
  * Filename              :   re_timer_init_callback.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_int_callback.h"

bool t500ms_Elapsed = false;
bool t5sec_Elapsed = false;

/**
 * @brief Timer period elapsed callback
 * this function handles the interupt callbacks of timer3
 * @param htim3_t Timer handle TypeDef
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{ 
    if(htim -> Instance == TIM2)
    {
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
        t500ms_Elapsed = true;
    }
    else if(htim -> Instance == TIM3)
    {
        t5sec_Elapsed = true;
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/